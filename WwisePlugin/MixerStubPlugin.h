#pragma once

#include <AK/Wwise/AudioPlugin.h>

#include "AK\Tools\Common\AkFNVHash.h"

class MixerStub
	: public AK::Wwise::DefaultAudioPluginImplementation, 
	public AK::Wwise::IPluginMediaConverter
{
public:
	MixerStub();
	~MixerStub();

	// AK::Wwise::IPluginBase
	virtual void Destroy();

	// AK::Wwise::IAudioPlugin
	virtual void SetPluginPropertySet( AK::Wwise::IPluginPropertySet * in_pPSet );

	virtual HINSTANCE GetResourceHandle() const;
	virtual bool GetDialog( eDialog in_eDialog, UINT & out_uiDialogID, AK::Wwise::PopulateTableItem *& out_pTable ) const;
	virtual bool WindowProc( eDialog in_eDialog, HWND in_hWnd, UINT in_message, WPARAM in_wParam, LPARAM in_lParam, LRESULT & out_lResult );

    virtual bool GetBankParameters( const GUID & in_guidPlatform, AK::Wwise::IWriteData* in_pDataWriter ) const;

	virtual bool Help( HWND in_hWnd, eDialog in_eDialog, LPCWSTR in_szLanguageCode ) const;

	static const short CompanyID;
	static const short PluginID;

	/////////////////////RealSpace3D
	virtual void SetPluginObjectMedia(AK::Wwise::IPluginObjectMedia * in_pObjectMedia);
	AK::Wwise::IPluginObjectMedia * m_pObjMedia;

	virtual AK::Wwise::IPluginMediaConverter* GetPluginMediaConverterInterface();

	virtual AK::Wwise::ConversionResult ConvertFile(
		const GUID & in_guidPlatform,					
		const BasePlatformID & in_basePlatform,			
		LPCWSTR in_szSourceFile,						
		LPCWSTR in_szDestFile,							
		AkUInt32 in_uSampleRate,						
		AkUInt32 in_uBlockLength,						
		AK::Wwise::IProgress* in_pProgress,				
		AK::Wwise::IWriteString* io_pError						
		) override;

	virtual ULONG GetCurrentConversionSettingsHash(const GUID & in_guidPlatform, AkUInt32 in_uSampleRate, AkUInt32 in_uBlockLength) override;


private:

	AK::Wwise::IPluginPropertySet * m_pPSet;
	HWND m_hwndPropView;
};
