//////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include ".\res\resource.h"
#include "MixerStubAttachmentPlugin.h"
#include ".\Help\TopicAlias.h"
#include <AK/Tools/Common/AkAssert.h>

using namespace AK;
using namespace Wwise;


// Bind non static text UI controls to properties for property view
AK_BEGIN_POPULATE_TABLE(MixerStubAttachmentPopTable)
AK_END_POPULATE_TABLE()

// These IDs must be the same as those specified in the plug-in's XML definition file.
// Note that there are restrictions on the values you can use for CompanyID, and PluginID
// must be unique for the specified CompanyID. Furthermore, these IDs are persisted
// in project files. NEVER CHANGE THEM or existing projects will not recognize this Plug-in.
// Be sure to read the SDK documentation regarding Plug-ins XML definition files.
const short MixerStubAttachment::CompanyID = AKCOMPANYID_AUDIOKINETIC;
const short MixerStubAttachment::PluginID = 501;

static LPCWSTR szBypass	= L"Bypass";
static LPCWSTR szGameDefinedParams = L"Game-Defined Params";

static LPCWSTR szReverbLength = L"Reverb Length";
static LPCWSTR szMaxNRefl = L"Late Reflection Order";
static LPCWSTR szEarlyNRefl = L"Early Reflection Order";

static LPCWSTR szRoomWidth = L"Room Size X";
static LPCWSTR szRoomLen = L"Room Size Y";
static LPCWSTR szRoomHeight = L"Room Size Z";

static LPCWSTR szRoomCenterX = L"Room Center X";
static LPCWSTR szRoomCenterY = L"Room Center Y";
static LPCWSTR szRoomCenterZ = L"Room Center Z";

static LPCWSTR szReflLeft = L"Room Reflection Coeff Left";
static LPCWSTR szReflRight = L"Room Reflection Coeff Right";
static LPCWSTR szReflFront = L"Room Reflection Coeff Front";
static LPCWSTR szReflBack = L"Room Reflection Coeff Back";
static LPCWSTR szReflFloor = L"Room Reflection Coeff Floor";
static LPCWSTR szReflCeil = L"Room Reflection Coeff Ceiling";

static LPCWSTR szMinRange = L"Sound Source Min-Range";
static LPCWSTR szMaxRange = L"Sound Source Max-Range";

static LPCWSTR szAzimuth = L"Preview Azimuth";
static LPCWSTR szElevation = L"Preview Elevation";
static LPCWSTR szDist = L"Preview Distance";

static 	LPCWSTR szBFastSpatial = L"Fast Spatialization";
static 	LPCWSTR szBSmoothDoppler = L"Smooth Doppler";

// Constructor
MixerStubAttachment::MixerStubAttachment()
	: m_pPSet( NULL )
	, m_hwndPropView( NULL )
{
}

// Destructor
MixerStubAttachment::~MixerStubAttachment()
{
}

// Implement the destruction of the Wwise source plugin.
void MixerStubAttachment::Destroy()
{
	delete this;
}

// Set internal values of the property set (allow persistence)
void MixerStubAttachment::SetPluginPropertySet( IPluginPropertySet * in_pPSet )
{
	m_pPSet = in_pPSet;
}

// Take necessary action on property changes. 
// Note: user also has the option of catching appropriate message in WindowProc function.
void MixerStubAttachment::NotifyPropertyChanged( const GUID & in_guidPlatform, LPCWSTR in_szPropertyName )
{
}

// Get access to UI resource handle.
HINSTANCE MixerStubAttachment::GetResourceHandle() const
{
	AFX_MANAGE_STATE( AfxGetStaticModuleState() );
	return AfxGetStaticModuleState()->m_hCurrentResourceHandle;
}

// Set the property names to UI control binding populated table.
bool MixerStubAttachment::GetDialog( eDialog in_eDialog, UINT & out_uiDialogID, PopulateTableItem *& out_pTable ) const
{
	AKASSERT( in_eDialog == SettingsDialog );

	out_uiDialogID = 0;
	out_pTable = NULL;

	return false;
}

// Standard window function, user can intercept what ever message that is of interest to him to implement UI behavior.
bool MixerStubAttachment::WindowProc( eDialog in_eDialog, HWND in_hWnd, UINT in_message, WPARAM in_wParam, LPARAM in_lParam, LRESULT & out_lResult )
{
	switch ( in_message )
	{
	case WM_INITDIALOG:
		m_hwndPropView = in_hWnd;
		break;
	case WM_DESTROY:
		m_hwndPropView = NULL;
		break;
	}

	out_lResult = 0;
	return false;
}

// Store current plugin settings into banks when asked to.
bool MixerStubAttachment::GetBankParameters( const GUID & in_guidPlatform, AK::Wwise::IWriteData* in_pDataWriter ) const
{
	CComVariant varProp;

	// Pack parameters in bank 
	// IMPORTANT NOTE: they need to be written and read on the AudioEngine side in the same order
	m_pPSet->GetValue( in_guidPlatform, szBypass, varProp );	in_pDataWriter->WriteBool( varProp.boolVal != 0 );
	m_pPSet->GetValue(in_guidPlatform, szGameDefinedParams, varProp);	in_pDataWriter->WriteBool(varProp.boolVal != 0);

	m_pPSet->GetValue(in_guidPlatform, szReverbLength, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szMaxNRefl, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);					
	m_pPSet->GetValue(in_guidPlatform, szEarlyNRefl, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);					


	m_pPSet->GetValue(in_guidPlatform, szRoomWidth, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szRoomLen, varProp);		in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szRoomHeight, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);

	m_pPSet->GetValue(in_guidPlatform, szRoomCenterX, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szRoomCenterY, varProp);		in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szRoomCenterZ, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);

	m_pPSet->GetValue(in_guidPlatform, szReflLeft, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szReflRight, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szReflFront, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szReflBack, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szReflFloor, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szReflCeil, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);

	m_pPSet->GetValue(in_guidPlatform, szMinRange, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szMaxRange, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);

	m_pPSet->GetValue(in_guidPlatform, szAzimuth, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szElevation, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szDist, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);

	m_pPSet->GetValue(in_guidPlatform, szBFastSpatial, varProp);	in_pDataWriter->WriteBool(varProp.boolVal != 0);
	m_pPSet->GetValue(in_guidPlatform, szBSmoothDoppler, varProp);	in_pDataWriter->WriteBool(varProp.boolVal != 0);
	
	return true;
}

// Implement online help when the user clicks on the "?" icon .
bool MixerStubAttachment::Help( HWND in_hWnd, eDialog in_eDialog, LPCWSTR in_szLanguageCode ) const
{
	AFX_MANAGE_STATE( ::AfxGetStaticModuleState() ) ;
/*
	if ( in_eDialog == AK::Wwise::IAudioPlugin::SettingsDialog )
		::SendMessage( in_hWnd, WM_AK_PRIVATE_SHOW_HELP_TOPIC, ONLINEHELP::VoiceCompressor_Properties, 0 );
	else
		return false;
*/
	return true;
}
