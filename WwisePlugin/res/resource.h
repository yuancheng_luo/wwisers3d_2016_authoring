//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MixerStub.rc
//
#define IDD_MIXER_STUB                  104
#define IDC_CHECK_BYPASS                21046
#define IDC_CHECK_CROSSCANCEL           21047
#define IDC_EDIT_HEAD_SIZE              21048
#define IDC_EDIT_TORSO_SIZE             21049
#define IDC_EDIT_NECK_SIZE              21050
#define IDC_EDIT_HRTF_TYPE              21051
#define IDC_EDIT_SCALEFAC               21052
#define IDC_EDIT_HRTF_TYPE2             21054

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        21003
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         21047
#define _APS_NEXT_SYMED_VALUE           21000
#endif
#endif
