// Gain.cpp : Defines the initialization routines for the DLL.
//
#include "stdafx.h"
#include ".\res\resource.h"
#include "MixerStubPlugin.h"
#include ".\Help\TopicAlias.h"
#include "..\..\SDK\samples\SoundEnginePlugin RS3D\MixerStubFXAttachmentParams.h"
#include <iostream>
#include <vector>

using namespace AK;
using namespace Wwise;

// Property names
static LPCWSTR szBypassBus = L"Bypass Bus";
static LPCWSTR szCrossCancel = L"Cross Cancel";
static LPCWSTR szHeadSize = L"Head Size";
static LPCWSTR szTorsoSize = L"Torso Size";
static LPCWSTR szNeckSize = L"Neck Size";
static LPCWSTR szHRTFType = L"HRTF Type";
static LPCWSTR szScaleFac = L"Scale Factor";


AK_BEGIN_POPULATE_TABLE(MixerStubProp)
AK_POP_ITEM(IDC_CHECK_BYPASS, szBypassBus)
AK_POP_ITEM(IDC_CHECK_CROSSCANCEL, szCrossCancel)
AK_POP_ITEM(IDC_EDIT_HEAD_SIZE, szHeadSize)
AK_POP_ITEM(IDC_EDIT_TORSO_SIZE, szTorsoSize)
AK_POP_ITEM(IDC_EDIT_NECK_SIZE, szNeckSize)
AK_POP_ITEM(IDC_EDIT_HRTF_TYPE, szHRTFType)
AK_POP_ITEM(IDC_EDIT_SCALEFAC, szScaleFac)
AK_END_POPULATE_TABLE()

// These IDs must be the same as those specified in the plug-in's XML definition file.
// Note that there are restrictions on the values you can use for CompanyID, and PluginID
// must be unique for the specified CompanyID. Furthermore, these IDs are persisted
// in project files. NEVER CHANGE THEM or existing projects will not recognize this Plug-in.
// Be sure to read the SDK documentation regarding Plug-ins XML definition files.
const short MixerStub::CompanyID = AKCOMPANYID_AUDIOKINETIC;
const short MixerStub::PluginID = AKEFFECTID_MIXERSTUB;

//const short MixerStub::PluginID = 246;

// Table of display name resources ( one for each property )
struct DisplayNameInfo
{
	LPCWSTR wszPropName;
	UINT    uiDisplayName;
};

MixerStub::MixerStub() : m_pPSet( NULL ) , m_hwndPropView( NULL )
{
}

MixerStub::~MixerStub()
{
}

void MixerStub::Destroy()
{
	delete this;
}

void MixerStub::SetPluginPropertySet( IPluginPropertySet * in_pPSet )
{
	m_pPSet = in_pPSet;
}

HINSTANCE MixerStub::GetResourceHandle() const
{
	AFX_MANAGE_STATE( AfxGetStaticModuleState() );
	return AfxGetStaticModuleState()->m_hCurrentResourceHandle;
}

bool MixerStub::GetDialog( eDialog in_eDialog, UINT & out_uiDialogID, PopulateTableItem *& out_pTable ) const
{
	AKASSERT( in_eDialog == SettingsDialog );

	out_uiDialogID = IDD_MIXER_STUB;
	out_pTable = MixerStubProp;

	return true;
}

bool MixerStub::WindowProc( eDialog in_eDialog, HWND in_hWnd, UINT in_message, WPARAM in_wParam, LPARAM in_lParam, LRESULT & out_lResult )
{
	switch ( in_message )
	{
	case WM_INITDIALOG:
		m_hwndPropView = in_hWnd;
		break;
	case WM_DESTROY:
		m_hwndPropView = NULL;
		break;
	}

	out_lResult = 0;
	return false;
}

bool MixerStub::GetBankParameters( const GUID & in_guidPlatform, AK::Wwise::IWriteData* in_pDataWriter ) const
{
	CComVariant varProp;

	m_pPSet->GetValue(in_guidPlatform, szBypassBus, varProp );	in_pDataWriter->WriteBool(varProp.boolVal != 0 );
	m_pPSet->GetValue(in_guidPlatform, szCrossCancel, varProp);	in_pDataWriter->WriteBool(varProp.boolVal != 0);

	m_pPSet->GetValue(in_guidPlatform, szHeadSize, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szTorsoSize, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szNeckSize, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);
	m_pPSet->GetValue(in_guidPlatform, szHRTFType, varProp);	in_pDataWriter->WriteInt32(varProp.intVal);
	m_pPSet->GetValue(in_guidPlatform, szScaleFac, varProp);	in_pDataWriter->WriteReal32(varProp.fltVal);

    return true;
}

bool MixerStub::Help( HWND in_hWnd, eDialog in_eDialog, LPCWSTR in_szLanguageCode ) const
{
	AFX_MANAGE_STATE( ::AfxGetStaticModuleState() ) ;

	/*
	if ( in_eDialog == AK::Wwise::IAudioPlugin::SettingsDialog )
		::SendMessage( in_hWnd, WM_AK_PRIVATE_SHOW_HELP_TOPIC, ONLINEHELP::MixerStub_Properties, 0 );
	else
		return false;
		*/

	return true;	
}

void MixerStub::SetPluginObjectMedia(AK::Wwise::IPluginObjectMedia * in_pObjectMedia) {
	m_pObjMedia = in_pObjectMedia;
	//Adds license key and HRTF files to Wwise manager


	//Get Wwise plugin path

//	// Via env variables
//	char *tmp = getenv("WWISEROOT");
//	std::string wwise_path = std::string(tmp);
//
//#if defined(_M_X64) || defined(__amd64__)
//	std::string sub_wwise_path = "\\Authoring\\x64\\Release\\bin\\plugins\\";
//#else
//	std::string sub_wwise_path = "\\Authoring\\Win32\\Release\\bin\\plugins\\";
//#endif
//	std::string plugin_dir_path = wwise_path + sub_wwise_path + "VisiSonics\\";

////	Via working directory
	std::string plugin_dir_path = "plugins\\VisiSonics\\";



	//License key (media id 0)
	std::string lic_key_path = plugin_dir_path + "lic_key.txt";
	std::wstring lic_key_path_w = std::wstring(lic_key_path.begin(), lic_key_path.end());
	if (!m_pObjMedia->SetMediaSource(lic_key_path_w.c_str(), 0, true))
		LOGE("MixerStub::SetPluginObjectMedia Failed to set lic_key media-source");
	m_pObjMedia->InvalidateMediaSource(0);

	//HRTF files (media id 1-6)
	std::vector<std::string> HRTF_file_name;
	HRTF_file_name.push_back("hrtf-cp048-r2.icb");
	HRTF_file_name.push_back("hrtf-cp021-r2.icb");
	HRTF_file_name.push_back("hrtf-cp165-r2.icb");
	HRTF_file_name.push_back("hrtf-andre-r1.icb");
	HRTF_file_name.push_back("hrtf-virgn-r1.icb");
	HRTF_file_name.push_back("hrtf-custom-r1.icb");

	for (int i = 0; i < HRTF_file_name.size(); ++i){
		std::string HRTF_path = plugin_dir_path + "Data\\" + HRTF_file_name[i];
		std::wstring HRTF_path_w = std::wstring(HRTF_path.begin(), HRTF_path.end());
		if (!m_pObjMedia->SetMediaSource(HRTF_path_w.c_str(), 1 + i, true))
			LOGE(std::string("MixerStub::SetPluginObjectMedia Failed to set HRTF media-source " + SSTR(i)).c_str() );
		m_pObjMedia->InvalidateMediaSource(1 + i);
	}
	

	//BACCH front and back stereo filter files (media id 7)
	std::string BACCH_stereo_front_filter_path = plugin_dir_path + "Data\\BACCH_filters_stereo_front.bin";
	std::wstring BACCH_stereo_front_filter_path_w  = std::wstring(BACCH_stereo_front_filter_path.begin(), BACCH_stereo_front_filter_path.end());
	if(!m_pObjMedia->SetMediaSource(BACCH_stereo_front_filter_path_w.c_str(), 1 + HRTF_file_name.size() + 0, true))
		LOGE("MixerStub::SetPluginObjectMedia Failed to set filter 1 media-source");
	m_pObjMedia->InvalidateMediaSource(1 + HRTF_file_name.size() + 0);

	std::string BACCH_stereo_back_filter_path = plugin_dir_path + "Data\\BACCH_filters_stereo_back.bin";
	std::wstring BACCH_stereo_back_filter_path_w = std::wstring(BACCH_stereo_back_filter_path.begin(), BACCH_stereo_back_filter_path.end());
	if (!m_pObjMedia->SetMediaSource(BACCH_stereo_back_filter_path_w.c_str(), 1 + HRTF_file_name.size() + 1, true))
		LOGE("MixerStub::SetPluginObjectMedia Failed to set filter 2 media-source");
	m_pObjMedia->InvalidateMediaSource(1 + HRTF_file_name.size() + 1);
}

AK::Wwise::IPluginMediaConverter* MixerStub::GetPluginMediaConverterInterface()
{
	return this;
}

AK::Wwise::ConversionResult MixerStub::ConvertFile(
	const GUID & in_guidPlatform,					///< The unique ID of the custom platform being converted for.
	const BasePlatformID & in_basePlatform,			///< The unique ID of the base platform being converted for.
	LPCWSTR in_szSourceFile,						///< Source File to convert data from.
	LPCWSTR in_szDestFile,							///< DestinationFile, must be created by the plug-in.
	AkUInt32 in_uSampleRate,						///< The target sample rate for the converted file, passing 0 will default to the platform default
	AkUInt32 in_uBlockLength,						///< The block length, passing 0 will default to the platform default
	AK::Wwise::IProgress* in_pProgress,				///< Optional Progress Bar controller.
	AK::Wwise::IWriteString* io_pError							///< Optional error string that can be displayed if ConversionResult is not successful
	){
	// Convert the original source to a converted file
	// At minimum, copy the original file to converted
	::CopyFile(in_szSourceFile, in_szDestFile, FALSE);
	return AK::Wwise::ConversionSuccess;
}

ULONG MixerStub::GetCurrentConversionSettingsHash(const GUID & in_guidPlatform, AkUInt32 in_uSampleRate, AkUInt32 in_uBlockLength)
{
	AK::FNVHash32 hashFunc;

	// Generate a Hash from effect parameters that have an influence on the conversion
	// Take the source file name
	CString szInputFileName;
	m_pObjMedia->GetMediaSourceFileName(szInputFileName.GetBuffer(_MAX_PATH), _MAX_PATH);
	szInputFileName.ReleaseBuffer();
	szInputFileName.MakeLower();
	return hashFunc.Compute((unsigned char *)(LPCTSTR)szInputFileName, szInputFileName.GetLength()*sizeof(TCHAR));
}

