// Gain.h : main header file for the Gain DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include ".\res\resource.h"		// main symbols


// CParametricEQApp
// See Gain.cpp for the implementation of this class
//

class CMixerStubApp : public CWinApp
{
public:
	CMixerStubApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

//XML without RTPC
/*
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (C) 2006 Audiokinetic Inc. -->
<PluginModule>
  <MixerPlugin Name="VisiSonics RealSpace3D" CompanyID="0" PluginID="146">
    <PluginInfo MenuPath="50-Wwise">
      <PlatformSupport>
        <Platform Name="Windows">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="Linux">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="WindowsPhone"/>
        <Platform Name="XboxOne"/>
        <Platform Name="Xbox360"/>
        <Platform Name="PS4"/>
        <Platform Name="Android">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="WiiUSW"/>
        <Platform Name="Mac">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="VitaSW"/>
        <Platform Name="iOS">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
      </PlatformSupport>
    </PluginInfo>
    
      <Properties>
      
      <Property Name="Bypass Bus" Type="bool">
        <UserInterface DisplayName="Bypass Bus" />
        <DefaultValue>false</DefaultValue>
        <AudioEnginePropertyID>0</AudioEnginePropertyID>
      </Property>
      
        <Property Name="Cross Cancel" Type="bool">
        <UserInterface DisplayName="Cross Cancel" />
        <DefaultValue>false</DefaultValue>
        <AudioEnginePropertyID>1</AudioEnginePropertyID>
      </Property>
        
      <Property Name="Head Size" Type="Real32">
        <UserInterface DisplayName="Head Radius (cm)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>100</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>10</DefaultValue>
        <AudioEnginePropertyID>2</AudioEnginePropertyID>
      </Property>

      <Property Name="Torso Size" Type="Real32">
        <UserInterface DisplayName="Torso Radius (cm)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>100</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>23</DefaultValue>
        <AudioEnginePropertyID>3</AudioEnginePropertyID>
      </Property>
      
      <Property Name="Neck Size" Type="Real32">
        <UserInterface DisplayName="Neck Height (cm)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>100</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>5</DefaultValue>
        <AudioEnginePropertyID>4</AudioEnginePropertyID>
      </Property>

      <Property Name="HRTF Type" Type="int32">
        <UserInterface DisplayName="HRTF Type" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="int32">
              <Min>0</Min>
              <Max>5</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>0</DefaultValue>
        <AudioEnginePropertyID>5</AudioEnginePropertyID>
      </Property>

      <Property Name="Scale Factor" Type="Real32">
        <UserInterface DisplayName="Scale Factor (m / game unit)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>1000</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>1</DefaultValue>
        <AudioEnginePropertyID>6</AudioEnginePropertyID>
      </Property>

    </Properties>
      
    <Attachables>
      <Attachable Name="AttachableMixerInput" CompanyID="0" PluginID="501">
        
        <Properties>
          
          <Property Name="Bypass" Type="bool">
            <UserInterface DisplayName="Bypass" />
            <DefaultValue>false</DefaultValue>
            <AudioEnginePropertyID>0</AudioEnginePropertyID>
          </Property>
          
          <Property Name="Game-Defined Params" Type="bool">
            <UserInterface DisplayName="Enable Game-Defined Params (non-mixer-attachment)" />
            <DefaultValue>true</DefaultValue>
            <AudioEnginePropertyID>1</AudioEnginePropertyID>
          </Property>


          
          <Property Name="Reverb Length" Type="Real32">
            <UserInterface DisplayName="Reverb Length (ms) (0 for auto)" />
            <AudioEnginePropertyID>2</AudioEnginePropertyID>
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>2000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
          </Property>
          
        <Property Name="Late Reflection Order" Type="Real32">
            <UserInterface DisplayName="Late Reflection Order (0 for auto)" />
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>3</AudioEnginePropertyID>
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>32</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
          </Property>
            
          <Property Name="Early Reflection Order" Type="Real32">
            <UserInterface DisplayName="Early Reflection Order" />
            <DefaultValue>3</DefaultValue>
            <AudioEnginePropertyID>4</AudioEnginePropertyID>
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>4</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
          </Property>

          <Property Name="Room Size X" Type="Real32">
            <UserInterface DisplayName="Room Size X (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>10</DefaultValue>
            <AudioEnginePropertyID>5</AudioEnginePropertyID>
          </Property>
          
          <Property Name="Room Size Y" Type="Real32">
            <UserInterface DisplayName="Room Size Y (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>10</DefaultValue>
            <AudioEnginePropertyID>6</AudioEnginePropertyID>
          </Property>
              
          <Property Name="Room Size Z" Type="Real32">
            <UserInterface DisplayName="Room Size Z (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>10</DefaultValue>
            <AudioEnginePropertyID>7</AudioEnginePropertyID>
          </Property>

          <Property Name="Room Center X" Type="Real32">
            <UserInterface DisplayName="Room Center X (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-10000</Min>
                  <Max>10000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>8</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Center Y" Type="Real32">
            <UserInterface DisplayName="Room Center Y (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-10000</Min>
                  <Max>10000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>9</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Center Z" Type="Real32">
            <UserInterface DisplayName="Room Center Z (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-10000</Min>
                  <Max>10000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>10</AudioEnginePropertyID>
          </Property>



          <Property Name="Room Reflection Coeff Left" Type="Real32">
            <UserInterface DisplayName="Room Refl. Coeff. Left (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>11</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Right" Type="Real32">
            <UserInterface DisplayName="Room Refl. Coeff. Right (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>12</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Front" Type="Real32">
            <UserInterface DisplayName="Room Refl. Coeff. Front (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>13</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Back" Type="Real32">
            <UserInterface DisplayName="Room Refl. Coeff. Back (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>14</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Floor" Type="Real32">
            <UserInterface DisplayName="Room Refl. Coeff. Floor (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>15</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Ceiling" Type="Real32">
            <UserInterface DisplayName="Room Refl. Coeff. Ceiling (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>16</AudioEnginePropertyID>
          </Property>

          <Property Name="Sound Source Min-Range" Type="Real32">
            <UserInterface DisplayName="Min Cutoff Range (m)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>17</AudioEnginePropertyID>
          </Property>

          <Property Name="Sound Source Max-Range" Type="Real32">
            <UserInterface DisplayName="Max Cutoff Range (m)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>100000</DefaultValue>
            <AudioEnginePropertyID>18</AudioEnginePropertyID>
          </Property>

          <Property Name="Preview Azimuth" Type="Real32">
            <UserInterface DisplayName="Preview Azimuth (deg.)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-180</Min>
                  <Max>180</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>19</AudioEnginePropertyID>
          </Property>

          <Property Name="Preview Elevation" Type="Real32">
            <UserInterface DisplayName="Preview Elevation (deg.)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-90</Min>
                  <Max>90</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>20</AudioEnginePropertyID>
          </Property>

          <Property Name="Preview Distance" Type="Real32">
            <UserInterface DisplayName="Preview Distance (Game Unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>1</DefaultValue>
            <AudioEnginePropertyID>21</AudioEnginePropertyID>
          </Property>
          
         <Property Name="Fast Spatialization" Type="bool">
           <UserInterface DisplayName="Fast Spatialization" />
           <DefaultValue>false</DefaultValue>
            <AudioEnginePropertyID>22</AudioEnginePropertyID>
         </Property>

        <Property Name="Smooth Doppler" Type="bool">
           <UserInterface DisplayName="Smooth Doppler" />
           <DefaultValue>false</DefaultValue>
            <AudioEnginePropertyID>23</AudioEnginePropertyID>
         </Property>
          
        </Properties>

      </Attachable>
    </Attachables>
  </MixerPlugin>
</PluginModule>
*/

//XML with RTPC
/*
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (C) 2006 Audiokinetic Inc. -->
<PluginModule>
  <MixerPlugin Name="VisiSonics RealSpace3D" CompanyID="0" PluginID="146">
    <PluginInfo MenuPath="50-Wwise">
      <PlatformSupport>
        <Platform Name="Windows">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="Linux">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="WindowsPhone"/>
        <Platform Name="XboxOne"/>
        <Platform Name="Xbox360"/>
        <Platform Name="PS4"/>
        <Platform Name="Android">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="WiiUSW"/>
        <Platform Name="Mac">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
        <Platform Name="VitaSW"/>
        <Platform Name="iOS">
          <CanReferenceDataFile>true</CanReferenceDataFile>
        </Platform>
      </PlatformSupport>
    </PluginInfo>
    
      <Properties>
      
      <Property Name="Bypass Bus" Type="bool" SupportRTPCType = "Exclusive" ForceRTPCCurveSegmentShape = "Constant">
        <UserInterface DisplayName="Bypass Bus" />
        <DefaultValue>false</DefaultValue>
        <AudioEnginePropertyID>0</AudioEnginePropertyID>
      </Property>
      
        <Property Name="Cross Cancel" Type="bool" SupportRTPCType = "Exclusive" ForceRTPCCurveSegmentShape = "Constant">
        <UserInterface DisplayName="Cross Cancel" />
        <DefaultValue>false</DefaultValue>
        <AudioEnginePropertyID>1</AudioEnginePropertyID>
      </Property>
        
      <Property Name="Head Size" Type="Real32">
        <UserInterface DisplayName="Head Radius (cm)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>100</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>10</DefaultValue>
        <AudioEnginePropertyID>2</AudioEnginePropertyID>
      </Property>

      <Property Name="Torso Size" Type="Real32">
        <UserInterface DisplayName="Torso Radius (cm)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>100</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>23</DefaultValue>
        <AudioEnginePropertyID>3</AudioEnginePropertyID>
      </Property>
      
      <Property Name="Neck Size" Type="Real32">
        <UserInterface DisplayName="Neck Height (cm)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>100</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>5</DefaultValue>
        <AudioEnginePropertyID>4</AudioEnginePropertyID>
      </Property>

      <Property Name="HRTF Type" Type="int32">
        <UserInterface DisplayName="HRTF Type" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="int32">
              <Min>0</Min>
              <Max>5</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>0</DefaultValue>
        <AudioEnginePropertyID>5</AudioEnginePropertyID>
      </Property>

      <Property Name="Scale Factor" Type="Real32">
        <UserInterface DisplayName="Scale Factor (m / game unit)" />
        <Restrictions>
          <ValueRestriction>
            <Range Type="Real32">
              <Min>1</Min>
              <Max>1000</Max>
            </Range>
          </ValueRestriction>
        </Restrictions>
        <DefaultValue>1</DefaultValue>
        <AudioEnginePropertyID>6</AudioEnginePropertyID>
      </Property>

    </Properties>
      
    <Attachables>
      <Attachable Name="AttachableMixerInput" CompanyID="0" PluginID="501">
        
        <Properties>
          
          <Property Name="Bypass" Type="bool" SupportRTPCType = "Exclusive" ForceRTPCCurveSegmentShape = "Constant">
            <UserInterface DisplayName="Bypass" />
            <DefaultValue>false</DefaultValue>
            <AudioEnginePropertyID>0</AudioEnginePropertyID>
          </Property>
          
          <Property Name="Game-Defined Params" Type="bool" SupportRTPCType = "Exclusive" ForceRTPCCurveSegmentShape = "Constant">
            <UserInterface DisplayName="Enable Game-Defined Params (non-mixer-attachment)" />
            <DefaultValue>true</DefaultValue>
            <AudioEnginePropertyID>1</AudioEnginePropertyID>
          </Property>


          
          <Property Name="Reverb Length" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Reverb Length (ms) (0 for auto)" />
            <AudioEnginePropertyID>2</AudioEnginePropertyID>
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>2000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
          </Property>
          
        <Property Name="Late Reflection Order" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Late Reflection Order (0 for auto)" />
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>3</AudioEnginePropertyID>
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>32</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
          </Property>
            
          <Property Name="Early Reflection Order" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Early Reflection Order" />
            <DefaultValue>3</DefaultValue>
            <AudioEnginePropertyID>4</AudioEnginePropertyID>
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>4</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
          </Property>

          <Property Name="Room Size X" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Size X (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>10</DefaultValue>
            <AudioEnginePropertyID>5</AudioEnginePropertyID>
          </Property>
          
          <Property Name="Room Size Y" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Size Y (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>10</DefaultValue>
            <AudioEnginePropertyID>6</AudioEnginePropertyID>
          </Property>
              
          <Property Name="Room Size Z" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Size Z (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>10</DefaultValue>
            <AudioEnginePropertyID>7</AudioEnginePropertyID>
          </Property>

          <Property Name="Room Center X" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Center X (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-10000</Min>
                  <Max>10000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>8</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Center Y" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Center Y (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-10000</Min>
                  <Max>10000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>9</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Center Z" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Center Z (game unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-10000</Min>
                  <Max>10000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>10</AudioEnginePropertyID>
          </Property>



          <Property Name="Room Reflection Coeff Left" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Refl. Coeff. Left (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>11</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Right" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Refl. Coeff. Right (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>12</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Front" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Refl. Coeff. Front (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>13</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Back" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Refl. Coeff. Back (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>14</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Floor" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Refl. Coeff. Floor (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>15</AudioEnginePropertyID>
          </Property>
          <Property Name="Room Reflection Coeff Ceiling" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Room Refl. Coeff. Ceiling (%)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>75</DefaultValue>
            <AudioEnginePropertyID>16</AudioEnginePropertyID>
          </Property>

          <Property Name="Sound Source Min-Range" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Min Cutoff Range (m)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>17</AudioEnginePropertyID>
          </Property>

          <Property Name="Sound Source Max-Range" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Max Cutoff Range (m)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>0</Min>
                  <Max>100000</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>100000</DefaultValue>
            <AudioEnginePropertyID>18</AudioEnginePropertyID>
          </Property>

          <Property Name="Preview Azimuth" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Preview Azimuth (deg.)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-180</Min>
                  <Max>180</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>19</AudioEnginePropertyID>
          </Property>

          <Property Name="Preview Elevation" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Preview Elevation (deg.)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>-90</Min>
                  <Max>90</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>0</DefaultValue>
            <AudioEnginePropertyID>20</AudioEnginePropertyID>
          </Property>

          <Property Name="Preview Distance" Type="Real32" SupportRTPCType = "Exclusive">
            <UserInterface DisplayName="Preview Distance (Game Unit)" />
            <Restrictions>
              <ValueRestriction>
                <Range Type="Real32">
                  <Min>1</Min>
                  <Max>300</Max>
                </Range>
              </ValueRestriction>
            </Restrictions>
            <DefaultValue>1</DefaultValue>
            <AudioEnginePropertyID>21</AudioEnginePropertyID>
          </Property>
          
         <Property Name="Fast Spatialization" Type="bool" SupportRTPCType = "Exclusive" ForceRTPCCurveSegmentShape = "Constant">
           <UserInterface DisplayName="Fast Spatialization" />
           <DefaultValue>false</DefaultValue>
            <AudioEnginePropertyID>22</AudioEnginePropertyID>
         </Property>

        <Property Name="Smooth Doppler" Type="bool" SupportRTPCType = "Exclusive" ForceRTPCCurveSegmentShape = "Constant">
           <UserInterface DisplayName="Smooth Doppler" />
           <DefaultValue>false</DefaultValue>
            <AudioEnginePropertyID>23</AudioEnginePropertyID>
         </Property>
          
        </Properties>

      </Attachable>
    </Attachables>
  </MixerPlugin>
</PluginModule>
*/